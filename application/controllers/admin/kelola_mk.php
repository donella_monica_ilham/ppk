<?php

class kelola_mk extends CI_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->model('penentuan_model');
		$this->load->library('session', 'table');
	}

	public function index()
	{
        $penampung['q'] = $this->penentuan_model->get();
        $this->load->view("admin/kelola_mk",$penampung);
	}
}

