<?php

class rekap_penugasandsn extends CI_Controller {
    public function __construct(){
		parent::__construct();
		$this->load->model('model_rekap_penugasandsn');
		$this->load->library('session', 'table');
	}

	public function index()
	{
        $penampung['q'] = $this->model_rekap_penugasandsn->get();
        $this->load->view("admin/view_rekap_penugasandsn",$penampung);
	}
}

