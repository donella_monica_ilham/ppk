<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body id="page-top">

<?php $this->load->view("admin/_partials/navbar.php") ?>

<div id="wrapper">

	<?php $this->load->view("admin/_partials/sidebar.php") ?>

	<div id="content-wrapper">

		<div class="container-fluid">
		<h2>Rekap Penugasan Dosen</h2>
		<!-- <div class="container my-auto"> -->
      	<span>PRODI : <?php echo SEKPROD_SI ." " ?></span>
	  <div class="form-group row">
 		<label class="col-md-1 col-form-label">PERIODE</label>
 		<div class="col-md-0">
  			<select class="form-control" id="category_name" name="category_name">
  		 <option selected="0">Ganjil </option>
		   <option selected="0">Genap </option>
		   <!-- Ganjil/ Genap  -->
   			<?php foreach($cats as $cat) : ?>
    		<option value="<?php echo $cat->cat_id;?>"> <?php echo $cat->cat_name; ?></option>
   <?php endforeach; ?>
  </select>
 </div>
</div>
		    <!-- Content Header (Page header) -->
     
<section class="content-header">
		
		   </section>
		
		
		   <!-- Main content -->
			
	   <section class="content">
			  
	   <div class="row">
				
	   <div class="col-xs-12">
		
				  
	   <div class="box">
	 
		
				   <!-- /.box-header -->
					
	   <div class="box-body">
					  
	   <table id="example1" class="table table-bordered table-striped">
						
	   <thead>
												
	   <th>Smt</th>
							
	   <th>Total Mhs</th>

	   <th>Kode MK</th>

	   <th>Nama MK</th>

	   <th>SKS</th>

	   <th>Kelas</th>

	   <th>Jenis</th>

	   <th>Nama Dosen</th>
							
	   <th>Status Gabung</th>
	   
					   </thead>			
	   <tbody>
	   <?php 
	   foreach($q AS $row){?>
      <tr>
	  
         <td><?php echo $row->smt?> </td>
         <td><?php echo $row->total_mhs?> </td>
		 <td><?php echo $row->kode_mk?> </td>
         <td><?php echo $row->nama_mk?> </td>
		 <td><?php echo $row->sks?> </td>
         <td><?php echo $row->kelas?> </td>
		 <td><?php echo $row->jenis?> </td>
         <td><?php echo $row->nama_dosen?> </td>
         <td><?php echo $row->status_gabung?> </td>
	  </tr>
			   <?php } ?>
							
	   <!-- <tr>
			  
	   <td><?php echo $smt; ?></td>
       <td><?php echo $total_mhs; ?></td> 
	   <td><?php echo $kode_mk; ?></td>
	   <td><?php echo $nama_mk; ?></td>
	   <td><?php echo $sks; ?></td>
	   <td><?php echo $kelas; ?></td>
       <td><?php echo $jenis; ?></td>
       <td><?php echo $nama_dosen; ?></td>
       <td><?php echo $status_gabung; ?></td>
	   </tr> -->
	  
		<tr>					  
	   <td>
	   <div class='container'>
							<button type="button" class="btn btn-outline-success">Save</button>
							</div>
							</td>
						   </tr>
						   <tr>
							</tr>
					   </tbody>
					   
					   
					 </table>
					 
					 
				   </div>

				   
		
				   <!-- /.box-body -->
				 </div>
				 
		
				 <!-- /.box -->
			   </div>
		
			   <!-- /.col -->
			 </div>
		
			 <!-- /.row -->
		   </section>
		   
		  
		   <!-- /.content -->
		
		   <!-- DataTables -->
		   <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-wp-preserve="%3Cscript%20src%3D%22%3C%3Fphp%20echo%20base_url()%3B%20%3F%3Eassets%2Fbower_components%2Fdatatables.net%2Fjs%2Fjquery.dataTables.min.js%22%3E%3C%2Fscript%3E" data-mce-resize="false" data-mce-placeholder="1" class="mce-object" width="20" height="20" alt="&lt;script&gt;" title="&lt;script&gt;" />
		   <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-wp-preserve="%3Cscript%20src%3D%22%3C%3Fphp%20echo%20base_url()%3B%20%3F%3Eassets%2Fbower_components%2Fdatatables.net-bs%2Fjs%2FdataTables.bootstrap.min.js%22%3E%3C%2Fscript%3E" data-mce-resize="false" data-mce-placeholder="1" class="mce-object" width="20" height="20" alt="&lt;script&gt;" title="&lt;script&gt;" />
		
		   <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-wp-preserve="%3Cscript%3E%0A%20%20%20%20%20%20%24(function()%20%7B%0A%20%20%20%20%20%20%20%20%24('%23example1').DataTable()%0A%20%20%20%20%20%20%20%20%24('%23example2').DataTable(%7B%0A%20%20%20%20%20%20%20%20%20%20'paging'%3A%20true%2C%0A%20%20%20%20%20%20%20%20%20%20'lengthChange'%3A%20false%2C%0A%20%20%20%20%20%20%20%20%20%20'searching'%3A%20false%2C%0A%20%20%20%20%20%20%20%20%20%20'ordering'%3A%20true%2C%0A%20%20%20%20%20%20%20%20%20%20'info'%3A%20true%2C%0A%20%20%20%20%20%20%20%20%20%20'autoWidth'%3A%20false%0A%20%20%20%20%20%20%20%20%7D)%0A%20%20%20%20%20%20%7D)%0A%20%20%20%20%3C%2Fscript%3E" data-mce-resize="false" data-mce-placeholder="1" class="mce-object" width="20" height="20" alt="&lt;script&gt;" title="&lt;script&gt;" />
	   
	
<?php $this->load->view("admin/_partials/scrolltop.php") ?>
<?php $this->load->view("admin/_partials/modal.php") ?>
<?php $this->load->view("admin/_partials/js.php") ?>
    
</body>
</html>
