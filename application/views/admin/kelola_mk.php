<!DOCTYPE html>
<html lang="en">
<head>
	<?php $this->load->view("admin/_partials/head.php") ?>
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<script type="text/javascript" src="js/jquery.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
</head>
<body id="page-top">

<?php $this->load->view("admin/_partials/navbar.php") ?>

<div id="wrapper">

	<?php $this->load->view("admin/_partials/sidebar.php") ?>

	<div id="content-wrapper">

		<div class="container-fluid">
		<h2>Penentuan MataKuliah</h2>
		<!-- <div class="container my-auto"> -->
      	<span>PRODI :<?php echo SEKPROD_SI ." " ?></span>
	  <div class="form-group row">
 		<label class="col-md-1 col-form-label">PERIODE:</label>
 		<div class="col-md-0">
  			<select class="form-control" id="category_name" name="category_name">
  		 <option selected="0">Ganjil </option>
		   <option selected="0">Genap </option>
		   <!-- Ganjil/ Genap  -->
   			<?php foreach($cats as $cat) : ?>
    		<option value="<?php echo $cat->cat_id;?>"> <?php echo $cat->cat_name; ?></option>
   <?php endforeach; ?>
  </select>
 </div>
</div>
		    <!-- Content Header (Page header) -->
     
<section class="content-header">
			  
	   <ol class="breadcrumb">
				
	   <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		
				
	   <li><a href="#">Tables</a></li>
		
				
	   <li class="active">Data tables</li>
		
			 </ol>
		
		   </section>
		
		
		   <!-- Main content -->
			
	   <section class="content">
			  
	   <div class="row">
				
	   <div class="col-xs-12">
		
				  
	   <div class="box">
					
	   <div class="box-header">
	   <a href="<?php echo base_url(); ?>index.php/mahasiswa/tambah" class="btn btn-sm btn-success">Tambah</a>
	   </div>
	 
		
				   <!-- /.box-header -->
					
	   <div class="box-body">
					  
	   <table id="example1" class="table table-bordered table-striped">
						
	   <thead>
						  
	   <tr>
							
	   <th>#</th>					
	   <th>Smt</th>
							
	   <th>Kode MK</th>
		
							
	   <th>Nama MK</th>

	   <th>SKS</th>

	   <th>Jenis</th>
		
							
	   <th>Action</th>
	   
		
						 </tr>
					   </thead>			
	   <tbody>
	   <?php 
	   foreach($q AS $row){?>
      <tr>
	  <td></td>
         <td><?php echo $row->smt?> </td>
         <td><?php echo $row->kode_mk?> </td>
		 <td><?php echo $row->nama_mk?> </td>
		 <td><?php echo $row->sks?> </td>
		 <td><?php echo $row->jenis?> </td>
	  </tr>
			   <?php } ?>
							
	   <!-- <tr>
			  
	   <td><?php echo $smt; ?></td>
		
							  
	   <td><?php echo $kode_mk; ?></td>
		
							  
	   <td><?php echo $nama_mk; ?></td>
		
							  
	   <td><?php echo $sks; ?></td>

	   <td><?php echo $jenis; ?></td>
	   </tr> -->
	  
		<tr>					  
	   <td>
	   <div class='container'>
							<a href="#" class="btn btn-xs btn-warning">Edit</a>
							<a href="#" class="btn btn-xs btn-danger">Del</a>
							<a href="#" class="btn btn-xs btn-info">Detail</a>
							<a href="#" class="btn btn-success btn-lg">Tombol</a>
					  	    <button><a href="<?php echo base_url(); ?>index.php/mahasiswa/tambah" class="btn btn-sm btn-success">Lanjut</a></button>				   
							<button type="button" class="btn btn-outline-primary">Primary</button>
							<button type="button" class="btn btn-outline-secondary">Secondary</button>
							<button type="button" class="btn btn-outline-success">Success</button>
							<button type="button" class="btn btn-outline-info">Info</button>
							<button type="button" class="btn btn-outline-warning">Warning</button>
							<button type="button" class="btn btn-outline-danger">Danger</button>
							<button type="button" class="btn btn-outline-dark">Dark</button>
							<button type="button" class="btn btn-outline-light text-dark">Light</button>
							</div>
							</td>
						   </tr>
						   <tr>
							</tr>
					   </tbody>
					   
					   
					 </table>
					 
					 
				   </div>

				   
		
				   <!-- /.box-body -->
				 </div>
				 
		
				 <!-- /.box -->
			   </div>
		
			   <!-- /.col -->
			 </div>
		
			 <!-- /.row -->
		   </section>
		   
		  
		   <!-- /.content -->
		
		   <!-- DataTables -->
		   <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-wp-preserve="%3Cscript%20src%3D%22%3C%3Fphp%20echo%20base_url()%3B%20%3F%3Eassets%2Fbower_components%2Fdatatables.net%2Fjs%2Fjquery.dataTables.min.js%22%3E%3C%2Fscript%3E" data-mce-resize="false" data-mce-placeholder="1" class="mce-object" width="20" height="20" alt="&lt;script&gt;" title="&lt;script&gt;" />
		   <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-wp-preserve="%3Cscript%20src%3D%22%3C%3Fphp%20echo%20base_url()%3B%20%3F%3Eassets%2Fbower_components%2Fdatatables.net-bs%2Fjs%2FdataTables.bootstrap.min.js%22%3E%3C%2Fscript%3E" data-mce-resize="false" data-mce-placeholder="1" class="mce-object" width="20" height="20" alt="&lt;script&gt;" title="&lt;script&gt;" />
		
		   <img src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" data-wp-preserve="%3Cscript%3E%0A%20%20%20%20%20%20%24(function()%20%7B%0A%20%20%20%20%20%20%20%20%24('%23example1').DataTable()%0A%20%20%20%20%20%20%20%20%24('%23example2').DataTable(%7B%0A%20%20%20%20%20%20%20%20%20%20'paging'%3A%20true%2C%0A%20%20%20%20%20%20%20%20%20%20'lengthChange'%3A%20false%2C%0A%20%20%20%20%20%20%20%20%20%20'searching'%3A%20false%2C%0A%20%20%20%20%20%20%20%20%20%20'ordering'%3A%20true%2C%0A%20%20%20%20%20%20%20%20%20%20'info'%3A%20true%2C%0A%20%20%20%20%20%20%20%20%20%20'autoWidth'%3A%20false%0A%20%20%20%20%20%20%20%20%7D)%0A%20%20%20%20%20%20%7D)%0A%20%20%20%20%3C%2Fscript%3E" data-mce-resize="false" data-mce-placeholder="1" class="mce-object" width="20" height="20" alt="&lt;script&gt;" title="&lt;script&gt;" />
	   
	
<?php $this->load->view("admin/_partials/scrolltop.php") ?>
<?php $this->load->view("admin/_partials/modal.php") ?>
<?php $this->load->view("admin/_partials/js.php") ?>
    
</body>
</html>
