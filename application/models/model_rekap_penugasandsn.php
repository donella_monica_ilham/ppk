<?php
class model_rekap_penugasandsn extends CI_Model {

    public function __construct(){ //function , yang dijalankan pertama kali
        $this->load->database(); //koneksi database
    }

    public function get ($id= '') {      
        if($id == ''){
            $query = $this->db->get('rekap_dosen');
            return $query->result();
        } else {
            $this->db->where('id', $id);
            $query = $this->db->get('rekap_dosen');
            return $query->row();
        }
    }
}