<?php
class penentuan_model extends CI_Model {

    public function __construct(){ //function , yang dijalankan pertama kali
        $this->load->database(); //koneksi database
    }

    public function get ($id= '') {      
        if($id == ''){
            $query = $this->db->get('penentuan1');
            return $query->result();
        } else {
            $this->db->where('id', $id);
            $query = $this->db->get('penentuan1');
            return $query->row();
        }
    }
}